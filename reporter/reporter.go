package reporter

import (
	"encoding/json"
	"fmt"
	"net"
	"net/http"
	"sync"
	"time"

	"hedwig/cache"
	"hedwig/common"
	"hedwig/config"

	"github.com/parnurzeal/gorequest"
	"k8s.io/client-go/pkg/api/v1"
	"k8s.io/client-go/pkg/api"
)

type EventDetail struct {
	Operation   string    `json:"operation"`
	Operator    string    `json:"operator"`
	ClusterUUID string    `json:"cluster_uuid"`
	ClusterName string    `json:"cluster_name"`
	Source      string    `json:"source"`
	Event       *v1.Event `json:"event"`
}

type Event struct {
	ID           string      `json:"id"`
	ResourceType string      `json:"resource_type"`
	ResourceID   string      `json:"resource_id"`
	Time         int64       `json:"time"`
	LogLevel     int         `json:"log_level"`
	Detail       EventDetail `json:"detail,omitempty"`
}

func (evt *Event) String() string {
	return fmt.Sprintf("%s/%s %s", evt.ResourceType, evt.ResourceID, evt.Detail.Operation)
}

type EventReporter struct {
	stopped  bool
	ch       chan *Event
	wg       sync.WaitGroup
	seq      int64
	request  *gorequest.SuperAgent
	debug    bool
	region   *cache.Region
	updateAt time.Time
	Retry    map[string]*v1.Event
	Logger   *common.Logger
}

func (r *EventReporter) Stop() {
	if r.stopped {
		return
	}
	r.stopped = true
}

func (r *EventReporter) Report(evt *v1.Event) {
	if r.stopped {
		return
	}
	if evt == nil {
		r.Logger.Error("Event is nil?")
		return
	}
	event := r.Build(evt)
	if event.Time > r.seq {
		r.seq = event.Time
	} else {
		r.seq++
		event.Time = r.seq
	}

	select {
	case r.ch <- event:
	default:
		r.Logger.Infof("Drop event %s as select failed: %s", evt.UID, evt.InvolvedObject)
	}
}

func (r *EventReporter) Build(evt *v1.Event) *Event {
	logLevel := 0
	if evt.Type == api.EventTypeWarning {
		logLevel = 1
	}
	var operator string
	if evt.Source.Component != "" {
		if evt.Source.Host != "" {
			operator = fmt.Sprintf("%s@%s", evt.Source.Component, evt.Source.Host)
		} else {
			operator = evt.Source.Component
		}
	}
	return &Event{
		ID:           string(evt.UID),
		ResourceType: evt.InvolvedObject.Kind,
		ResourceID:   string(evt.InvolvedObject.UID),
		Time:         evt.LastTimestamp.Unix() * 1000000, // in microsecond
		LogLevel:     logLevel,
		Detail: EventDetail{
			Operation:   evt.Reason,
			Operator:    operator,
			Source:      "kubernetes",
			ClusterUUID: r.region.UUID,
			ClusterName: r.region.Name,
			Event:       evt,
		},
	}
}

func (r *EventReporter) loop() {
	r.wg.Add(1)
	defer r.wg.Done()
	defer close(r.ch)

	var eventQueue []*Event
	retry := 0
	for !r.stopped {
		maxEvent := config.GlobalConfig.Event.MaxCount
		reportBefore := time.Now().Add(time.Second * time.Duration(config.GlobalConfig.Event.MaxInterval))
		for len(eventQueue) < maxEvent && time.Now().Before(reportBefore) {
			select {
			case event := <-r.ch:
				if event == nil {
					break
				}
				eventQueue = append(eventQueue, event)
			case <-time.After(reportBefore.Sub(time.Now())):
				break
			}
		}
		if len(eventQueue) == 0 {
			continue
		}
		if err := r.report(eventQueue); err != nil {
			retry++
			if retry < config.GlobalConfig.Event.MaxRetry {
				time.Sleep(time.Second)
				continue
			}
			r.Logger.Error("Report abort!")
		}
		eventQueue = eventQueue[:0]
		retry = 0
	}

	if len(eventQueue) == 0 {
		return
	}
	r.report(eventQueue)
}

func (r *EventReporter) Join() {
	r.Logger.Infof("Reporter has been stopped, wait for goroutines exit")
	r.wg.Wait()
	r.Logger.Infof("Reporter has been stopped, all goroutines exit, join finished")
}

func (r *EventReporter) report(events []*Event) error {
	body, err := json.Marshal(events)
	if err != nil {
		r.Logger.Error(err.Error())
		return err
	}
	url := fmt.Sprintf("%s/%s/logs/events", config.GlobalConfig.Tiny.Endpoint, config.GlobalConfig.Tiny.ApiVersion)
	r.Logger.Infof("Try to report %d events to %s", len(events), url)
	resp, _, errs := r.request.Post(url).Send(string(body)).End()
	if len(errs) != 0 {
		r.Logger.Errorf("Report events failed with request error: %s", errs[0])
		return errs[0]
	}
	if resp.StatusCode > 300 {
		r.Logger.Errorf("Report events failed with status code %d", resp.StatusCode)
		return fmt.Errorf("post failed")
	}
	r.Logger.Infof("Report %d events succeeded with status code %d", len(events), resp.StatusCode)
	return nil
}
func (r *EventReporter) dial(network, addr string) (net.Conn, error) {
	dial := net.Dialer{
		Timeout:   time.Duration(config.GlobalConfig.Tiny.Timeout) * time.Second,
		KeepAlive: 60 * time.Second,
	}
	conn, err := dial.Dial(network, addr)
	r.Logger.Debuglf(8, "Connect done, use %s", conn.LocalAddr().String())
	return conn, err
}

func New(region *cache.Region) *EventReporter {
	reporter := &EventReporter{
		stopped:  false,
		ch:       make(chan *Event, config.GlobalConfig.Event.MaxCount*3),
		debug:    config.GlobalConfig.Hedwig.LogLevel > 8,
		region:   region,
		updateAt: time.Now().UTC(),
		Logger:   common.NewLogger(map[string]string{"region": region.Name + "/" + region.UUID + "/" + region.Namespace}),
	}
	reporter.request = gorequest.New().SetDebug(reporter.debug).
		Set("Content-Type", "application/json").
		Timeout(time.Second * time.Duration(config.GlobalConfig.Tiny.Timeout))
	reporter.request.Transport = &http.Transport{Dial: reporter.dial}
	go reporter.loop()
	return reporter
}
