FROM golang:1.8

COPY . $GOPATH/src/hedwig
WORKDIR $GOPATH/src/hedwig

RUN cp ./run/source/sources.list /etc/apt/sources.list && \
    apt-get update && \
    apt-get install -y supervisor logrotate cron curl


RUN go install && \
    mkdir -p /var/log/mathilde && \
    mkdir -p /etc/supervisor/conf.d/ && \
    mkdir -p /etc/cron.hourly/ && \
    cp ./run/supervisord/supervisord.conf /etc/supervisor/ && \
    cp ./run/supervisord//hedwig.conf /etc/supervisor/conf.d/ && \
    cp ./run/log/logrotate.conf /etc/logrotate.conf && \
    cp ./run/log/logrotate.sh /etc/cron.hourly/logrotate && \
    rm -rf /etc/cron.daily/* && \
    chmod a+x /etc/cron.hourly/logrotate

CMD ["supervisord", "-c", "/etc/supervisor/supervisord.conf", "--nodaemon", "-d", "/go/"]
