OS = Linux
VERSION = 0.0.1

build:
	docker-compose -p hedwig -f run/script/hedwig-compose.yaml build

up:
	docker-compose -p hedwig -f run/script/hedwig-compose.yaml up

compose:
	docker-compose -p hedwig -f run/script/hedwig-compose.yaml build
	docker-compose -p hedwig -f run/script/hedwig-compose.yaml up

help:
	@$(ECHO) "Targets:"
	@$(ECHO) "compose           - docker compose to build and start this project"
