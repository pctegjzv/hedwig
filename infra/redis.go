package infra

import (
	"fmt"
	"strings"
	"time"

	"hedwig/common"
	"hedwig/config"

	aredis "github.com/liticer/go-redis-client"
	"gopkg.in/redis.v5"
)

var (
	Redis RedisClient
)

// RedisClient Redis client wrapper for enigma
type RedisClient struct {
	read   *aredis.Client
	write  *aredis.Client
	Logger *common.Logger
}

func getRedisReadConfig() aredis.Options {
	clientType := aredis.ClientType(config.GlobalConfig.Redis.TypeReader)
	hosts := make([]string, len(config.GlobalConfig.Redis.HostReader))
	for idx, host := range config.GlobalConfig.Redis.HostReader {
		port := config.GlobalConfig.Redis.PortReader[idx]
		hosts = append(hosts, fmt.Sprintf("%s:%s", host, port))
	}
	hosts = common.RemoveEmptyString(hosts)

	return aredis.Options{
		Type:      clientType,
		Hosts:     hosts,
		Database:  config.GlobalConfig.Redis.DBNameReader,
		Password:  config.GlobalConfig.Redis.DBPasswordReader,
		KeyPrefix: config.GlobalConfig.Redis.KeyPrefixReader,
		PoolSize:  config.GlobalConfig.Redis.MaxConnectionsReader,
	}
}

func getRedisWriteConfig() aredis.Options {
	clientType := aredis.ClientType(config.GlobalConfig.Redis.TypeWriter)
	hosts := make([]string, len(config.GlobalConfig.Redis.HostWriter))

	for idx, host := range config.GlobalConfig.Redis.HostWriter {
		port := config.GlobalConfig.Redis.PortWriter[idx]
		hosts = append(hosts, fmt.Sprintf("%s:%s", host, port))
	}
	hosts = common.RemoveEmptyString(hosts)

	return aredis.Options{
		Type:      clientType,
		Hosts:     hosts,
		Database:  config.GlobalConfig.Redis.DBNameWriter,
		Password:  config.GlobalConfig.Redis.DBPasswordWriter,
		KeyPrefix: config.GlobalConfig.Redis.KeyPrefixWriter,
		PoolSize:  config.GlobalConfig.Redis.MaxConnectionsWriter,
	}
}

// GetRedis returns the redis cache instance. a simple singleton.
func GetRedis(logger *common.Logger) *RedisClient {
	if Redis.read != nil && Redis.write != nil {
		return &Redis
	}
	if logger == nil {
		logger = common.NewLogger(map[string]string{"role": "redis"})
	}

	r, err := NewAlaudaRedis(getRedisReadConfig(), getRedisWriteConfig(), logger)
	if err != nil {
		panic(err.Error())
	}
	Redis = *r
	return r
}

// NewAlaudaRedis construtor based on alauda redis client
func NewAlaudaRedis(opts aredis.Options, writerOpts aredis.Options, logger *common.Logger) (*RedisClient, error) {
	reader := aredis.NewClient(opts)
	writer := reader
	if len(writerOpts.Hosts) > 0 {
		writer = aredis.NewClient(writerOpts)
	}
	client := &RedisClient{
		read:   reader,
		write:  writer,
		Logger: logger,
	}
	if err := client.read.Ping().Err(); err != nil {
		return nil, err
	}
	return client, nil
}

func (red *RedisClient) Ping() error {
	_, err := red.read.Ping().Result()
	return err
}

func (red *RedisClient) Incr(key string) (int64, error) {
	return red.write.Incr(key).Result()
}

func (red *RedisClient) Expire(key string, duration time.Duration) error {
	_, err := red.write.Expire(key, duration).Result()
	return err
}

func (red *RedisClient) TTL(key string) (time.Duration, error) {
	return red.read.TTL(key).Result()
}

const noTTLConst = time.Second * -1

func (red *RedisClient) NoTTLValue() time.Duration {
	return noTTLConst
}

const expiredTTLConst = time.Second * -2

func (red *RedisClient) ExpiredTTLValue() time.Duration {
	return expiredTTLConst
}

// Set set a value to a key
func (red *RedisClient) Set(key string, value interface{}, expiration time.Duration) (string, error) {
	return red.write.Set(key, value, expiration).Result()
}

// SetNX set a value to a key
func (red *RedisClient) SetNX(key string, value interface{}, expiration time.Duration) (bool, error) {
	return red.write.SetNX(key, value, expiration).Result()
}

// Get get a key value
func (red *RedisClient) Get(key string) (string, error) {
	cmd := red.read.Get(key)
	return cmd.Val(), cmd.Err()
}

func (red *RedisClient) Del(key string) error {
	return red.write.Del(key).Err()
}

// LeftPop Get a value beginning of the list
func (red *RedisClient) LeftPop(key string) (string, error) {
	cmd := red.write.LPop(key)
	return cmd.Val(), cmd.Err()
}

// RightPush Push a value to the end of the list
func (red *RedisClient) RightPush(key string, value interface{}) (int64, error) {
	return red.write.RPush(key, value).Result()
}

// Publish publishes to a channel
func (red *RedisClient) Publish(channel string, value string) error {
	return red.write.Publish(channel, value).Err()
}

// Exists
func (red *RedisClient) Exists(key ...string) (int64, error) {
	return red.read.Exists(key...).Result()
}

// GetMatchCacheKeys fetch all keys match the patten in cache
func (red *RedisClient) GetMatchCacheKeys(pattern string) ([]string, error) {
	return red.read.ScanAll(pattern)
}

// GetMatchData retrieve list of data from cache which keys match the pattern
func (red *RedisClient) GetMatchData(pattern string) ([]string, error) {
	keys, err := red.GetMatchCacheKeys(pattern)
	if err != nil {
		return nil, err
	}
	if len(keys) == 0 {
		return []string{}, nil
	}

	var newKeys []string
	for _, key := range keys {
		newKeys = append(newKeys, strings.TrimPrefix(key, config.GlobalConfig.Redis.KeyPrefixReader))
	}

	var res []string

	if red.read.IsCluster() {
		pipe := red.read.Pipeline()
		for _, k := range newKeys {
			if err := pipe.Process(red.read.Get(k)); err != nil {
				return nil, err
			}
		}
		cmders, err := pipe.Exec()
		if err != nil {
			return nil, err
		}
		for _, cmder := range cmders {
			res = append(res, cmder.String())
		}
		return res, nil
	}

	vals, err := red.read.MGet(newKeys...).Result()

	if redis.Nil != err && nil != err {
		return nil, err
	}

	for _, item := range vals {
		if item != nil {
			res = append(res, fmt.Sprintf("%s", item))
		}
	}

	return res, err
}
