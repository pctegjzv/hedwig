package config

import (
	"os"

	"github.com/sirupsen/logrus"
	"github.com/vrischmann/envconfig"
)

var (
	GlobalConfig Config
)

type Config struct {
	Hedwig struct {
		Debug    bool `envconfig:"default=false"`
		LogLevel int  `envconfig:"default=1"`
	}

	Furion struct {
		Endpoint     string
		ApiVersion   string `envconfig:"default=v1"`
		Timeout      int    `envconfig:"default=3"`
		SyncPeriod   int    `envconfig:"default=60"`
		UpdatePeriod int    `envconfig:"default=300"`
	}

	Tiny struct {
		Endpoint   string
		ApiVersion string `envconfig:"default=v2"`
		Timeout    int    `envconfig:"default=3"`
	}

	Kubernetes struct {
		Timeout int `envconfig:"default=10"`
	}

	Redis struct {
		TypeWriter           string `envconfig:"default=normal"`
		TypeReader           string `envconfig:"default=normal"`
		HostWriter           []string
		HostReader           []string
		PortReader           []string
		PortWriter           []string
		DBNameWriter         int    `envconfig:"default=0,REDIS_DB_NAME_WRITER"`
		DBNameReader         int    `envconfig:"default=0,REDIS_DB_NAME_READER"`
		DBPasswordReader     string `envconfig:"optional"`
		DBPasswordWriter     string `envconfig:"optional"`
		MaxConnectionsReader int    `envconfig:"default=32"`
		MaxConnectionsWriter int    `envconfig:"default=32"`
		// KeyPrefix should keep the same, use Reader if not
		KeyPrefixWriter string `envconfig:"default=hedwig:"`
		KeyPrefixReader string `envconfig:"default=hedwig:"`
	}

	Event struct {
		MaxCount    int `envconfig:"default=100"`
		MaxInterval int `envconfig:"default=5"`
		MaxRetry    int `envconfig:"default=3"`
		MaxLatency  int `envconfig:"default=60"`
	}

	Lock struct {
		Name    string `envconfig:"default=controller:lock"`
		Timeout int    `envconfig:"default=120"`
	}

	Server struct {
		SleepPeriod int `envconfig:"default=20"`
	}

	Worker struct {
		SleepPeriod int `envconfig:"default=5"`
	}

	Controller struct {
		QueueName string `envconfig:"default=controller:queue"`
	}
}

func InitConfig() {
	err := GlobalConfig.LoadFromEnv()
	if err != nil {
		logrus.Errorf("Load config from env error : %s", err.Error())
		os.Exit(1)
	}
}

// LoadFromEnv load env to the Config struct, also check redis env host/port match.
func (conf *Config) LoadFromEnv() error {
	if err := envconfig.Init(conf); err != nil {
		return err
	}

	if conf.Redis.KeyPrefixReader != conf.Redis.KeyPrefixWriter {
		conf.Redis.KeyPrefixWriter = conf.Redis.KeyPrefixReader
	}

	return nil
}
