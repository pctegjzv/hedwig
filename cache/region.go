package cache

import (
	"encoding/json"
	"fmt"
	"sync"
	"time"

	"hedwig/common"
	"hedwig/config"

	"github.com/deckarep/golang-set"
	"github.com/Jeffail/gabs"
	"github.com/levigross/grequests"
	"k8s.io/apimachinery/pkg/runtime/schema"
	"k8s.io/client-go/kubernetes"
	"k8s.io/client-go/pkg/api"
	"k8s.io/client-go/rest"
	"k8s.io/apimachinery/pkg/apis/meta/v1"
)

const (
	KubernetesContainerManager = "KUBERNETES"
	// RegionPlatformV3 support new k8s
	RegionPlatformV3 = "v3"
	// RegionPlatformV4 support new region info and new k8s
	RegionPlatformV4 = "v4"
)

type FurionClient struct {
	Endpoint   string
	ApiVersion string
	Timeout    int
	Logger     *common.Logger
}

type KubernetesConfig struct {
	Endpoint string `json:"endpoint"`
	Token    string `json:"token"`
}

type Region struct {
	KubernetesClient *kubernetes.Clientset `json:"-"`
	UUID             string                `json:"uuid"`
	Name             string                `json:"name"`
	Namespace        string                `json:"namespace"`
	Namespaces       []string              `json:"namespaces"`
	InstanceID       string                `json:"instance_id"`
	Cached           bool                  `json:"cached"`
	Config           *KubernetesConfig     `json:"config"`
	Timeout          int                   `json:"timeout"`
	UpdatedAt        time.Time             `json:"updated_at"`
	Logger           *common.Logger        `json:"-"`
}

type RegionCache struct {
	lock    *sync.RWMutex
	regions map[string]*Region
	Logger  *common.Logger
}

func (r *Region) Clone() *Region {
	return &Region{
		KubernetesClient: r.KubernetesClient,
		UUID:             r.UUID,
		Name:             r.Name,
		Namespace:        r.Namespace,
		InstanceID:       r.InstanceID,
		Cached:           r.Cached,
		Config:           r.Config,
		Timeout:          r.Timeout,
		UpdatedAt:        r.UpdatedAt,
		Logger:           r.Logger,
	}
}

func (r *Region) GenerateRestConfig() *rest.Config {
	cf := &rest.Config{
		Host:        r.Config.Endpoint,
		BearerToken: r.Config.Token,
		Timeout:     time.Duration(config.GlobalConfig.Kubernetes.Timeout) * time.Second,
	}

	if cf.APIPath == "" {
		cf.APIPath = "/api"
	}
	if cf.GroupVersion == nil {
		cf.GroupVersion = &schema.GroupVersion{}
	}
	if cf.NegotiatedSerializer == nil {
		cf.NegotiatedSerializer = api.Codecs
	}
	cf.Insecure = true
	return cf
}

func (r *Region) GetNamespaces() ([]string, error) {
	if r.KubernetesClient == nil {
		r.KubernetesClient = kubernetes.NewForConfigOrDie(r.GenerateRestConfig())
	}
	list, err := r.KubernetesClient.Namespaces().List(v1.ListOptions{})
	if err != nil {
		return nil, err
	}
	namespaces := make([]string, len(list.Items))
	for index, item := range list.Items {
		namespaces[index] = item.Name
	}
	return namespaces, nil
}

func (rc *RegionCache) String() string {
	rc.lock.RLock()
	defer rc.lock.RUnlock()
	regions, err := json.MarshalIndent(rc.regions, "", "  ")
	if err != nil {
		panic(err)
	}
	return fmt.Sprintf("Regions[%d]:\n", len(rc.regions)) + string(regions)
}

func (rc *RegionCache) Keys() []string {
	rc.lock.RLock()
	defer rc.lock.RUnlock()
	var keys []string
	for key := range rc.regions {
		keys = append(keys, key)
	}
	return keys
}

func (rc *RegionCache) Get(k string) *Region {
	rc.lock.RLock()
	defer rc.lock.RUnlock()
	if val, ok := rc.regions[k]; ok {
		return val
	}
	return nil
}

func (rc *RegionCache) Set(k string, v *Region) {
	rc.lock.Lock()
	defer rc.lock.Unlock()
	if val, ok := rc.regions[k]; ok {
		v.Cached = val.Cached
	}
	rc.regions[k] = v
}

func (rc *RegionCache) check(k string) bool {
	rc.lock.RLock()
	defer rc.lock.RUnlock()
	if _, ok := rc.regions[k]; !ok {
		return false
	}
	return true
}

func (rc *RegionCache) delete(k string) {
	rc.lock.Lock()
	defer rc.lock.Unlock()
	delete(rc.regions, k)
}

func (rc *RegionCache) ListRegions() []*Region {
	var regions []*Region
	for _, key := range rc.Keys() {
		region := rc.Get(key)
		if region == nil {
			continue
		}
		for _, namespace := range region.Namespaces {
			r := region.Clone()
			r.Namespace = namespace
			regions = append(regions, r)
		}
	}
	return regions
}

func (rc *RegionCache) Watch() {
	client := &FurionClient{
		Endpoint:   config.GlobalConfig.Furion.Endpoint,
		ApiVersion: config.GlobalConfig.Furion.ApiVersion,
		Timeout:    config.GlobalConfig.Furion.Timeout,
		Logger:     rc.Logger,
	}
	for {
		regions, err := client.ListRegions()
		if err != nil {
			rc.Logger.Errorf("List regions error, sleep 10s, %v", err.Error())
			time.Sleep(time.Second * 15)
			continue
		}
		current := mapset.NewSet()
		previous := mapset.NewSet()
		for _, region := range regions {
			current.Add(region.UUID)
		}
		for _, key := range rc.Keys() {
			previous.Add(key)
		}
		insert := current.Difference(previous)
		update := current.Intersect(previous)
		delete := previous.Difference(current)
		for _, region := range regions {
			cached := rc.Get(region.UUID)
			if cached != nil {
				region.Namespaces = cached.Namespaces
			}
			if insert.Contains(region.UUID) {
				rc.Logger.Infof("Insert region %s/%s", region.Name, region.UUID)
				rc.Set(region.UUID, region)
			}
			if update.Contains(region.UUID) {
				rc.Logger.Infof("Update region %s/%s", region.Name, region.UUID)
				rc.Set(region.UUID, region)
			}
		}
		for _, key := range rc.Keys() {
			if delete.Contains(key) {
				region := rc.Get(key)
				rc.Logger.Infof("Delete region %s/%s", region.Name, region.UUID)
				rc.delete(key)
			}
		}

		for _, key := range rc.Keys() {
			region := rc.Get(key)
			if region == nil {
				continue
			}
			namespaces, err := region.GetNamespaces()
			if err != nil {
				rc.Logger.Debugf("Get namespace error, %s", err.Error())
				continue
			}
			region.Namespaces = namespaces
			rc.Set(region.UUID, region)
		}
		time.Sleep(time.Second * time.Duration(config.GlobalConfig.Furion.SyncPeriod))
	}
}

func (fc *FurionClient) ListRegions() (map[string]*Region, error) {
	url := fmt.Sprintf("%s/%s/regions", fc.Endpoint, fc.ApiVersion)
	response, err := grequests.Get(url, &grequests.RequestOptions{RequestTimeout: time.Duration(fc.Timeout) * time.Second})
	if err != nil {
		return nil, err
	}

	regionList, err := gabs.ParseJSON([]byte(response.String()))
	if err != nil {
		return nil, err
	}

	children, err := regionList.Children()
	if err != nil {
		return nil, err
	}

	var regions = map[string]*Region{}

	for _, child := range children {
		region, err := fc.generateRegion(child)
		if err != nil {
			fc.Logger.Errorf("Generate region error, %v", err.Error())
		}
		if region != nil {
			region.Timeout = 60
			regions[region.UUID] = region
		}
	}
	return regions, nil
}

func (fc *FurionClient) generateRegion(child *gabs.Container) (*Region, error) {
	// Generate region
	data, err := child.ChildrenMap()
	if err != nil {
		return nil, err
	}
	region := &Region{
		UUID:      data["id"].Data().(string),
		Name:      data["name"].Data().(string),
		Cached:    false,
		Config:    &KubernetesConfig{},
		UpdatedAt: time.Now().UTC(),
	}

	// Only kubernetes cluster is supported.
	manager, ok := data["container_manager"].Data().(string)
	if !ok {
		return nil, fmt.Errorf("no container manager found for region: %s/%s", region.Name, region.UUID)
	}
	if manager != KubernetesContainerManager {
		return nil, nil
	}

	// Only v3/v4 platform version is supported.
	platform, ok := data["platform_version"].Data().(string)
	if !ok {
		return nil, fmt.Errorf("no platform version found for region: %s/%s", region.Name, region.UUID)
	}
	if platform != RegionPlatformV3 && platform != RegionPlatformV4 {
		return nil, nil
	}

	// Generate paths.
	var paths []string
	if platform == RegionPlatformV3 {
		paths = []string{
			"features.kubernetes.endpoint",
			"features.kubernetes.token",
		}
	}
	if platform == RegionPlatformV4 {
		paths = []string{
			"attr.kubernetes.endpoint",
			"attr.kubernetes.token",
		}
	}

	// Get kubernetes config from json.
	ok = false
	body, err := gabs.ParseJSON([]byte(child.String()))
	if err != nil {
		return nil, err
	}
	if region.Config.Endpoint, ok = body.Path(paths[0]).Data().(string); !ok {
		return nil, fmt.Errorf("no %s found for region: %s/%s", paths[0], region.Name, region.UUID)
	}
	if region.Config.Token, ok = body.Path(paths[1]).Data().(string); !ok {
		return nil, fmt.Errorf("no %s found for region: %s/%s", paths[1], region.Name, region.UUID)
	}
	return region, nil
}

func New() *RegionCache {
	r := &RegionCache{
		lock:    new(sync.RWMutex),
		regions: make(map[string]*Region),
		Logger:  common.NewLogger(map[string]string{"role": "cache"}),
	}
	go r.Watch()
	return r
}
