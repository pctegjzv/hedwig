package provider

import (
	"sync"

	"hedwig/cache"
	"hedwig/common"

	"k8s.io/apimachinery/pkg/watch"
	"k8s.io/client-go/kubernetes"
	apiV1 "k8s.io/client-go/pkg/api/v1"
	metaV1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"time"
)

type EventProvider struct {
	clientset *kubernetes.Clientset
	wg        sync.WaitGroup
	ch        chan *apiV1.Event
	watcher   watch.Interface
	stopped   bool
	failed    int
	mutex     sync.Mutex
	region    *cache.Region
	Logger    *common.Logger
}

func (p *EventProvider) UpdateRegion(region *cache.Region) {
	p.mutex.Lock()
	defer p.mutex.Unlock()
	p.Logger.Debugf("Update region for provider")
	p.Logger.Debuglf(2, "Region config: %s, %s", region.Config.Endpoint, region.Config.Token)
	p.region = region
}

func (p *EventProvider) UpdateClient() {
	p.mutex.Lock()
	p.clientset = kubernetes.NewForConfigOrDie(p.region.GenerateRestConfig())
	p.mutex.Unlock()
	p.failed++
	p.Logger.Debugf("Update kubernetes client, sleep %ds", p.failed*30)
	time.Sleep(time.Second * time.Duration(p.failed*30))
}

func (p *EventProvider) Stop() {
	if p.stopped {
		return
	}
	p.stopped = true
	if p.watcher != nil {
		p.watcher.Stop()
	}
	p.Logger.Infof("Event provider has been stopped")
}

func (p *EventProvider) Join() {
	p.Logger.Infof("Provider has been stopped, wait for goroutines exit")
	p.wg.Wait()
	p.Logger.Infof("Provider has been stopped, all goroutines exit, join finished")
}

func (p *EventProvider) ResultChan() <-chan *apiV1.Event {
	return p.ch
}

func (p *EventProvider) loop() {
	p.wg.Add(1)
	defer p.wg.Done()
	defer close(p.ch)
	var err error
	var watcher watch.Interface
	p.mutex.Lock()
	var namespace = p.region.Namespace
	p.mutex.Unlock()
	for !p.stopped {
		p.UpdateClient()
		watcher, err = p.clientset.CoreV1().Events(namespace).Watch(metaV1.ListOptions{Watch: true})
		if err != nil {
			p.Logger.Errorf("Watch region failed, %s", err.Error())
			continue
		}
		p.watcher = watcher
		for watchEvent := range watcher.ResultChan() {
			apiEvent, ok := watchEvent.Object.(*apiV1.Event)
			if !ok {
				continue
			}
			p.Logger.Infof("Get event: %s/%s/%s %s", apiEvent.InvolvedObject.Namespace, apiEvent.InvolvedObject.Name, apiEvent.InvolvedObject.Kind, apiEvent.Reason)
			p.ch <- apiEvent
			p.failed = 0
		}
		watcher.Stop()
	}
}

func New(region *cache.Region) *EventProvider {
	provider := &EventProvider{
		clientset: kubernetes.NewForConfigOrDie(region.GenerateRestConfig()),
		ch:        make(chan *apiV1.Event),
		failed:    0,
		region:    region,
		Logger:    common.NewLogger(map[string]string{"region": region.Name + "/" + region.UUID + "/" + region.Namespace}),
	}
	go provider.loop()
	return provider
}
