package util

import (
	"sync"
	"time"

	"hedwig/common"
	"hedwig/infra"
)

// Locker allows (repeated) distributed locking.
type Locker struct {
	client  *infra.RedisClient
	key     string
	value   string
	timeout time.Duration
	mutex   sync.Mutex
	Logger  *common.Logger
}

// New creates a new distributed locker on a given key.
func NewLocker(client *infra.RedisClient, key string, value string, timeout int, logger *common.Logger) *Locker {
	locker := &Locker{
		client:  client,
		key:     key,
		value:   value,
		timeout: time.Duration(timeout) * time.Second,
		Logger:  logger,
	}
	go locker.Run()
	return locker
}

func (l *Locker) Run() {
	for {
		if !l.IsLocked() {
			l.Lock()
		} else {
			l.Refresh()
		}
		time.Sleep(l.timeout / 3)
	}
}

func (l *Locker) Lock() {
	l.mutex.Lock()
	defer l.mutex.Unlock()
	_, err := l.client.SetNX(l.key, l.value, l.timeout)
	if err != nil {
		l.Logger.Errorf("SetNX error, %s", err.Error())
		return
	}
}

func (l *Locker) IsLocked() bool {
	l.mutex.Lock()
	defer l.mutex.Unlock()
	value, err := l.client.Get(l.key)
	if err != nil {
		l.Logger.Errorf("Get error, %s", err.Error())
		return false
	}
	return value == l.value
}

func (l *Locker) Refresh() {
	l.mutex.Lock()
	defer l.mutex.Unlock()
	err := l.client.Expire(l.key, l.timeout)
	if err != nil {
		l.Logger.Errorf("Expire error, %s", err.Error())
		return
	}
	return
}
