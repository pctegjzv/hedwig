package util

import "time"

type Timer struct {
	Period        int
	LastTimeStamp time.Time
}

func (t *Timer) IsTimeUp() bool {
	if time.Now().UTC().Sub(t.LastTimeStamp).Seconds() > float64(t.Period) {
		t.LastTimeStamp = time.Now().UTC()
		return true
	}
	return false
}

func NewTimer(interval int) *Timer {
	return &Timer{
		Period:        interval,
		LastTimeStamp: time.Now().UTC(),
	}
}
