package controller

import (
	"time"

	"hedwig/cache"
	"hedwig/common"
	"hedwig/config"
	"hedwig/util"
)

type Server struct {
	Cache  *cache.RegionCache
	Redis  *Redis
	Locker *util.Locker
	Logger *common.Logger
}

func NewServer() *Server {
	s := &Server{
		Cache:  cache.New(),
		Logger: common.NewLogger(map[string]string{"role": "server"}),
	}
	s.Redis = NewRedis(s.Logger)
	s.Locker = util.NewLocker(s.Redis.Client, config.GlobalConfig.Lock.Name, HedwigInstanceUUID, config.GlobalConfig.Lock.Timeout, s.Logger)
	go s.Run()
	return s
}

func (m *Server) Run() {
	// period controls how often you look at the status of locker
	period := time.Second * time.Duration(config.GlobalConfig.Server.SleepPeriod)
	// timer controls how often to update region config in cache
	timer := util.NewTimer(config.GlobalConfig.Furion.UpdatePeriod)
	for {
		// Check if I have get the locker
		time.Sleep(period)
		if !m.Locker.IsLocked() {
			m.Logger.Debugf("I am not master, sleep")
			time.Sleep(period * 2)
			continue
		}
		m.Logger.Debugf("I am master, wow")

		// List all regions from furion and watched regions from cache
		regionsToWatch := m.Cache.ListRegions()
		regionsWatched, err := m.Redis.ListRegions()
		if err != nil {
			m.Logger.Errorf("List regions error, %s, continue", err.Error())
			continue
		}

		// Push region to cache if this region not watched
		m.Logger.Infof("All/Watched (%d/%d)", len(regionsToWatch), len(regionsWatched))
		matched := &cache.Region{}
		isTime := timer.IsTimeUp()
		for _, region := range regionsToWatch {
			matched = nil
			logger := GetLoggerByRegion(region, m.Logger.GetFields())
			for _, i := range regionsWatched {
				if i.UUID == region.UUID && i.Namespace == region.Namespace {
					matched = i
					break
				}
			}
			if matched != nil {
				if isTime {
					matched.Config = region.Config
					if err := m.Redis.SetRegion(matched); err != nil {
						logger.Errorf("Update region error, %s", err.Error())
					}
				}
				continue
			}
			if err := m.Redis.PushRegion(region); err != nil {
				logger.Errorf("Push region error, %s", err.Error())
			}
			logger.Infof("Push region succeeded")
		}

		// Delete region from cache if this region does not exist in furion
		for _, region := range regionsWatched {
			matched = nil
			logger := GetLoggerByRegion(region, m.Logger.GetFields())
			for _, i := range regionsToWatch {
				if i.UUID == region.UUID && i.Namespace == region.Namespace {
					matched = i
					break
				}
			}
			if matched != nil {
				continue
			}
			if err := m.Redis.DeleteRegion(region); err != nil {
				logger.Errorf("Delete region error, %s", err.Error())
			}
			logger.Infof("Delete region succeeded")
		}
	}
}
