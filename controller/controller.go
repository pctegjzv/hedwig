package controller

import (
	"fmt"

	"hedwig/cache"
	"hedwig/common"

	"k8s.io/apimachinery/pkg/util/uuid"
)

var HedwigInstanceUUID = string(uuid.NewUUID())

type Controller struct {
	Server *Server
	Worker *Worker
	Logger *common.Logger
}

func New() *Controller {
	return &Controller{
		Server: NewServer(),
		Worker: NewWorker(),
		Logger: common.NewLogger(map[string]string{"role": "controller"}),
	}
}

func GetRedisKey(cluster, namespace string) string {
	return "cluster:" + cluster + ":namespace:" + namespace
}

func GetLoggerByRegion(region *cache.Region, fields map[string]string) *common.Logger {
	fields["region"] = fmt.Sprintf("%s/%s/%s", region.Name, region.UUID, region.Namespace)
	return common.NewLogger(fields)
}
