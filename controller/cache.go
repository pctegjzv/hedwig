package controller

import (
	"encoding/json"
	"time"

	"hedwig/cache"
	"hedwig/common"
	"hedwig/config"
	"hedwig/infra"
)

type Redis struct {
	Client *infra.RedisClient
	Logger *common.Logger
}

func NewRedis(logger *common.Logger) *Redis {
	return &Redis{
		Client: infra.GetRedis(logger),
		Logger: logger,
	}
}

func (r *Redis) GetRegion(key string) (*cache.Region, error) {
	data, err := r.Client.Get(key)
	if err != nil {
		return nil, err
	}

	cached := &cache.Region{}
	if err := json.Unmarshal([]byte(data), cached); err != nil {
		return nil, err
	}
	return cached, nil
}

func (r *Redis) SetRegion(region *cache.Region) error {
	data, err := json.Marshal(*region)
	if err != nil {
		return err
	}

	key := GetRedisKey(region.UUID, region.Namespace)
	value := string(data)
	timeout := time.Duration(config.GlobalConfig.Lock.Timeout) * time.Second
	if _, err := r.Client.Set(key, value, timeout); err != nil {
		return err
	}
	return nil
}

func (r *Redis) RefreshRegion(region *cache.Region) error {
	key := GetRedisKey(region.UUID, region.Namespace)
	timeout := time.Duration(config.GlobalConfig.Lock.Timeout) * time.Second
	if err := r.Client.Expire(key, timeout); err != nil {
		return err
	}
	return nil
}

func (r *Redis) DeleteRegion(region *cache.Region) error {
	key := GetRedisKey(region.UUID, region.Namespace)
	if err := r.Client.Del(key); err != nil {
		return err
	}
	return nil
}

func (r *Redis) PushRegion(region *cache.Region) error {
	data, err := json.Marshal(*region)
	if err != nil {
		return err
	}

	if _, err := r.Client.RightPush(config.GlobalConfig.Controller.QueueName, data); err != nil {
		return err
	}
	return nil
}

func (r *Redis) PopRegion() (*cache.Region, error) {
	data, err := r.Client.LeftPop(config.GlobalConfig.Controller.QueueName)
	if err != nil {
		return nil, err
	}

	region := &cache.Region{}
	if err := json.Unmarshal([]byte(data), region); err != nil {
		return nil, err
	}
	return region, nil
}

func (r *Redis) ListRegions() ([]*cache.Region, error) {
	var regions []*cache.Region

	list, err := r.Client.GetMatchData(GetRedisKey("*", "*"))
	if err != nil {
		return regions, err
	}

	for _, item := range list {
		region := &cache.Region{}
		if err := json.Unmarshal([]byte(item), region); err != nil {
			continue
		}
		regions = append(regions, region)
	}
	return regions, nil
}
