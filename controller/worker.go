package controller

import (
	"sort"
	"time"

	"hedwig/cache"
	"hedwig/common"
	"hedwig/config"
	"hedwig/provider"
	"hedwig/reporter"
	"hedwig/util"
)

type Worker struct {
	Redis    *Redis
	Watchers map[string]*Watcher
	Logger   *common.Logger
}

type Watcher struct {
	Reporter *reporter.EventReporter
	Provider *provider.EventProvider
	stopped  bool
	Logger   *common.Logger
}

func NewWorker() *Worker {
	w := &Worker{
		Watchers: make(map[string]*Watcher),
		Logger:   common.NewLogger(map[string]string{"role": "worker"}),
	}
	w.Redis = NewRedis(w.Logger)
	go w.Run()
	return w
}

func (s *Worker) Run() {
	// period controls how often you look at watchers
	period := time.Second * time.Duration(config.GlobalConfig.Worker.SleepPeriod)
	for {
		// Remove stopped watchers
		for k, v := range s.Watchers {
			if v.stopped {
				s.Logger.Infof("Watcher (%s) has stopped, delete it", k)
				delete(s.Watchers, k)
			}
		}

		// Print watchers info
		s.Logger.Infof("Now watchers (%d), Instance UUID %s", len(s.Watchers), HedwigInstanceUUID)
		var keys []string
		for key := range s.Watchers {
			keys = append(keys, key)
		}
		sort.Strings(keys)
		for index, key := range keys {
			s.Logger.Infof("[%04d] %s", index, key)
		}

		// Pop a region from cache, prepare to watch
		region, err := s.Redis.PopRegion()
		if err != nil {
			if common.IsRedisNotFoundError(err) {
				s.Logger.Debugf("No region, sleep")
			} else {
				s.Logger.Errorf("Pop region error, %s, continue", err.Error())
			}
			time.Sleep(period * 4)
			continue
		}

		// Check if this region already watched
		logger := GetLoggerByRegion(region, s.Logger.GetFields())
		_, err = s.Redis.GetRegion(GetRedisKey(region.UUID, region.Namespace))
		if err != nil {
			if !common.IsRedisNotFoundError(err) {
				logger.Errorf("Get region error, %s,e", err.Error())
				continue
			}
		} else {
			logger.Debugf("Already watched, skip")
			continue
		}

		// Set flag to indicate this region is watched
		region.InstanceID = HedwigInstanceUUID
		region.Cached = true
		if err := s.Redis.SetRegion(region); err != nil {
			logger.Errorf("Set region error, %s", err.Error())
			continue
		}

		// Start watcher for this region
		s.StartWatcher(region)
		time.Sleep(period)
	}
}

func (s *Worker) StartWatcher(region *cache.Region) {
	logger := GetLoggerByRegion(region, s.Logger.GetFields())
	// Check if watcher already exists
	k := region.Name + ":" + region.UUID + ":" + region.Namespace
	if _, ok := s.Watchers[k]; ok {
		logger.Infof("Already has a watcher, skip")
		return
	}
	// Start new watcher for region
	r := reporter.New(region)
	p := provider.New(region)
	w := &Watcher{Reporter: r, Provider: p, Logger: s.Logger}
	s.StartDaemonForWatcher(w, region)
	s.Watchers[k] = w
	logger.Infof("Watcher started")
}

func (s *Worker) StartDaemonForWatcher(watcher *Watcher, region *cache.Region) {
	// Chain channel for provider and reporter
	go func() {
		for evt := range watcher.Provider.ResultChan() {
			watcher.Reporter.Report(evt)
		}
	}()

	// Refresh cache to reporter liveness for watcher
	go func() {
		logger := GetLoggerByRegion(region, s.Logger.GetFields())
		timer := util.NewTimer(config.GlobalConfig.Furion.UpdatePeriod)
		for {
			time.Sleep(time.Second * time.Duration(config.GlobalConfig.Lock.Timeout) / 3)
			logger.Debuglf(2, "Begin refresh region")
			watched, err := s.Redis.GetRegion(GetRedisKey(region.UUID, region.Namespace))
			if err != nil {
				if common.IsRedisNotFoundError(err) {
					logger.Debugf("Not watched by any instance, break")
					break
				} else {
					logger.Errorf("Get region from cache error, %s, continue", err.Error())
					continue
				}
			}
			if watched.InstanceID != HedwigInstanceUUID {
				logger.Infof("Not watched by this instance, break")
				break
			}
			if err := s.Redis.RefreshRegion(region); err != nil {
				logger.Errorf("Refresh region error, %s", err.Error())
			}
			if timer.IsTimeUp() {
				watcher.Provider.UpdateRegion(watched)
			}
			logger.Debuglf(2, "End refresh region")
		}
		logger.Infof("Begin stop provider/reporter")
		watcher.Reporter.Stop()
		watcher.Provider.Stop()
		watcher.Reporter.Join()
		watcher.Provider.Join()
		logger.Infof("End stop provider/reporter")
		watcher.stopped = true
	}()
}
