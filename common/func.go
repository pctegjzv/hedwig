package common

const RedisNotFoundError = "redis: nil"

func IsRedisNotFoundError(err error) bool {
	return err.Error() == RedisNotFoundError
}

// RemoveEmptyString remove empty string for a slice.
func RemoveEmptyString(list []string) []string {
	var result []string
	for _, v := range list {
		if v == "" {
			continue
		} else {
			result = append(result, v)
		}
	}
	return result
}
