package common

import (
	"hedwig/config"

	"github.com/sirupsen/logrus"
	"sort"
)

type Logger struct {
	fields map[string]string
	keys   []string
}

func (l *Logger) prefix() string {
	if len(l.fields) == 0 {
		return ""
	}
	prefix := "["
	for _, key := range l.keys {
		prefix += key + ":" + l.fields[key] + ", "
	}
	slice := []byte(prefix)
	slice = slice[:len(slice)-2]
	prefix = string(slice)
	return prefix + "]"
}

// Infof logs to the INFO log.
// Arguments are handled in the manner of fmt.Printf; a newline is appended if missing.
func (l *Logger) Infof(format string, args ...interface{}) {
	if l.prefix() != "" {
		format = l.prefix() + " " + format
	}
	logrus.Infof(format, args...)
}

// Info logs to the INFO log.
// Arguments are handled in the manner of fmt.Print; a newline is appended if missing.
func (l *Logger) Info(args ...interface{}) {
	if l.prefix() != "" {
		args = append([]interface{}{l.prefix()}, args)
	}
	logrus.Info(args...)
}

// Errorf logs to the ERROR, WARNING, and INFO logs.
// Arguments are handled in the manner of fmt.Printf; a newline is appended if missing.
func (l *Logger) Errorf(format string, args ...interface{}) {
	if l.prefix() != "" {
		format = l.prefix() + " " + format
	}
	logrus.Errorf(format, args...)
}

// Error logs to the ERROR, WARNING, and INFO logs.
// Arguments are handled in the manner of fmt.Printf; a newline is appended if missing.
func (l *Logger) Error(format string, args ...interface{}) {
	if l.prefix() != "" {
		args = append([]interface{}{l.prefix()}, args)
	}
	logrus.Error(args...)
}

// Debug logs to the DEBUG log.
// Arguments are handled in the manner of fmt.Print; a newline is appended if missing.
func (l *Logger) Debug(args ...interface{}) {
	if !config.GlobalConfig.Hedwig.Debug {
		return
	}
	if l.prefix() != "" {
		args = append([]interface{}{l.prefix()}, args)
	}
	logrus.Info(args...)
}

// Debugf logs to the Debug logs.
// Arguments are handled in the manner of fmt.Printf; a newline is appended if missing.
func (l *Logger) Debugf(format string, args ...interface{}) {
	if !config.GlobalConfig.Hedwig.Debug {
		return
	}
	if l.prefix() != "" {
		format = l.prefix() + " " + format
	}
	logrus.Infof(format, args...)
}

// Debuglf logs to the Debug logs.
// Arguments are handled in the manner of fmt.Printf; a newline is appended if missing.
func (l *Logger) Debuglf(level int, format string, args ...interface{}) {
	if !config.GlobalConfig.Hedwig.Debug {
		return
	}
	if config.GlobalConfig.Hedwig.LogLevel < level {
		return
	}
	if l.prefix() != "" {
		format = l.prefix() + " " + format
	}
	logrus.Infof(format, args...)
}

func (l *Logger) GetFields() map[string]string {
	fields := map[string]string{}
	for k, v := range l.fields {
		fields[k] = v
	}
	return fields
}

func NewLogger(fields map[string]string) *Logger {
	var keys []string
	for k := range fields {
		keys = append(keys, k)
	}
	sort.Strings(keys)
	return &Logger{fields: fields, keys: keys}
}
