package main

import (
	"flag"
	"net/http"

	"hedwig/config"
	"hedwig/controller"

	_ "net/http/pprof"
	"github.com/sirupsen/logrus"
)

func main() {
	flag.Parse()
	defer logrus.Info("Service exit.")
	config.InitConfig()

	logrus.Info("Service start.")

	controller.New()
	// Start server for debug
	http.ListenAndServe("0.0.0.0:8080", nil)
}
